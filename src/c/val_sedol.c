#include <ctype.h>
#include <stdlib.h>
#include <string.h>

int validate_sedol(const char *sedol)
{
    int d, sum, i;

    if (!sedol || strlen(sedol) != 7 || !isdigit(sedol[6]))
    {
        return 0;
    }

    for (sum = 0, i = 0; i < 6; ++i) {
        if (isupper(sedol[i]))
        {
            d = sedol[i] - 'A' + 10;
        }
        else if (isdigit(sedol[i]))
        {
            d = sedol[i] - '0';
        }
        else
        {
            return 0;
        }

        switch (i) {
        case 1:
        case 4:
            d *= 3;
            break;
        case 3:
            d *= 7;
            break;
        case 5:
            d *= 9;
            break;
        }

        sum += d;
    }

    sum %= 10;
    sum = 10 - sum;
    sum %= 10;

    if (sum != sedol[6] - '0')
    {
        return 0;
    }

    return 1;
}

