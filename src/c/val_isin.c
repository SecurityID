#include <ctype.h>
#include <stdlib.h>
#include <string.h>

int validate_isin(const char *isin)
{
    int d1, d2, sum, multiply, i;

    if (!isin || strlen(isin) != 12 || !isdigit(isin[11]))
    {
        return 0;
    }

    for (sum = 0, multiply = 1, i = 10; i > -1; --i) {
        switch (i) {
        case 0:
        case 1:
            if (isupper(isin[i]))
            {
                d1 = isin[i] - 'A' + 10;
            } 
            else
            {
                return 0;
            }
            break;
        default:
            if (isupper(isin[i]))
            {
                d1 = isin[i] - 'A' + 10;
            }
            else if (isdigit(isin[i]))
            {
                d1 = isin[i] - '0';
            }
            else
            {
                return 0;
            }
            break;
        }

        if (d1 < 10) {
            d1 *= (multiply ? 2 : 1);
            multiply = !multiply;
        } else {
            d2 = d1 / 10;
            d1 %= 10;
            d1 *= (multiply ? 2 : 1);
            d2 *= (multiply ? 1 : 2);
            sum += (d2 % 10) + (d2 / 10);
        }
        sum += (d1 % 10) + (d1 / 10);
    }

    sum %= 10;
    sum = 10 - sum;
    sum %= 10;

    if (sum != isin[11] - '0')
    {
        return 0;
    }

    return 1;
}

