#include <ctype.h>
#include <stdlib.h>
#include <string.h>

int validate_cusip(const char *cusip)
{
    int d1, d2, sum, multiply, i;

    if (!cusip || strlen(cusip) != 9 || !isdigit(cusip[8]))
    {
        return 0;
    }

    for (sum = 0, multiply = 1, i = 7; i > -1; --i) {
        if (i < 3) {
            if (isdigit(cusip[i]))
            {
                d1 = cusip[i] - '0';
            }
            else
            {
                return 0;
            }
        } else {
            if (isupper(cusip[i]))
            {
                d1 = cusip[i] - 'A' + 10;
            }
            else if (isdigit(cusip[i]))
            {
                d1 = cusip[i] - '0';
            }
            else
            {
                return 0;
            }
        }

        if (d1 < 10) {
            d1 *= (multiply ? 2 : 1);
            multiply = !multiply;
        } else {
            d2 = d1 / 10;
            d1 %= 10;
            d1 *= (multiply ? 2 : 1);
            d2 *= (multiply ? 1 : 2);
            sum += (d2 % 10) + (d2 / 10);
        }
        sum += (d1 % 10) + (d1 / 10);
    }

    sum %= 10;
    sum = 10 - sum;
    sum %= 10;

    if (sum != cusip[8] - '0')
    {
        return 0;
    }

    return 1;
}

