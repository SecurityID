require 'pathname'
require 'rake'

SRCDIR = "src"
LIBDIR = "lib"
OBJDIR = "obj"
BINDIR = "bin"

LIBNAME = "libSecurityID.so"

CSRCDIR = "#{SRCDIR}/c"

VALIDATIONS = ["cusip", "isin", "sedol"]
VAL_OBJECTS = VALIDATIONS.collect {|v| File.join(OBJDIR, "val_#{v}.o") }
ALL_PRODUCTS = VAL_OBJECTS + [File.join(LIBDIR, LIBNAME)]

def compile(output, *sources)
  opts = sources.pop if sources.last.is_a?(Hash)
  opts ||= {}
  sh "cc -c -o #{File.join(opts[:output_dir] || OBJDIR, output)} #{sources.join(' ')}"
end

def link(output, *sources)
  opts = sources.pop if sources.last.is_a?(Hash)
  opts ||= {}
  sh "cc -o #{File.join(opts[:output_dir] || LIBDIR, output)} #{sources.join(' ')}"
end

VALIDATIONS.each do |validation|
  src = File.join(CSRCDIR, "val_#{validation}.c")
  pathname = Pathname.new(src)
  out = pathname.basename.to_s.sub(/\.c$/, '.o')
  file "#{OBJDIR}/val_#{validation}.o" => [src] do
    compile(out, src)
  end
end

file "clib" => VAL_OBJECTS do
  link(LIBNAME, *VAL_OBJECTS) 
end

file "clean" do
  rm_rf ALL_PRODUCTS
end

task :default => "clib"
